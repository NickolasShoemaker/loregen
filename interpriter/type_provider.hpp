#ifndef TYPE_PROVIDER_HPP
#define TYPE_PROVIDER_HPP

#include <unordered_map>
#include <functional>

#include "lexical.hpp"

class type_factory{
public:

	static part_of_speach* Noun_provider(){
		part_of_speach* temp = new Noun();
		return temp;
	}
	static part_of_speach* Verb_provider(){
		part_of_speach* temp = new Verb();
		return temp;
	}
	static part_of_speach* Adjective_provider(){
		part_of_speach* temp = new Adjective();
		return temp;
	}
	static part_of_speach* Adverb_provider(){
		part_of_speach* temp = new Adverb();
		return temp;
	}
	static part_of_speach* Preposition_provider(){
		part_of_speach* temp = new Preposition();
		return temp;
	}
	static part_of_speach* Conjunction_provider(){
		part_of_speach* temp = new Conjunction();
		return temp;
	}
	static part_of_speach* Article_provider(){
		part_of_speach* temp = new Article();
		return temp;
	}
};

std::unordered_map<std::string, std::function<part_of_speach*()>> type_map = {	//std::function<part_of_speach()>
	{"Noun", 			type_factory::Noun_provider},
	{"Verb", 			type_factory::Verb_provider},
	{"Adjective", 		type_factory::Adjective_provider},
	{"Adverb", 			type_factory::Adverb_provider},
	{"Preposition", 	type_factory::Preposition_provider},
	{"Conjunction", 	type_factory::Conjunction_provider},
	{"Article", 		type_factory::Article_provider},
};

#endif // TYPE_PROVIDER_HPP