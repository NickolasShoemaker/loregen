#ifndef PARCER_HPP
#define PARCER_HPP

#include <iostream>
#include <functional>

#include "tokenizer.hpp"
#include "type_provider.hpp"

/* Grammer
* 
* variable assign Type -> create object and store in variable
* Type -> create object perform action delete object
* variable or variable.something -> reference object
* string -> print string
*/

constexpr int DA = -3;

static int transition_table[][7] = {
	//NUL, VAR, DER, ASS, TYP, NA , STR,
	   FA,   1,   4,  NA,   5,  DA,   6,

	   FA,  NA,  NA,   2,  NA,  NA,  NA,
	   FA,  NA,  NA,  NA,   3,  NA,  NA,
	   FA,  NA,  NA,  NA,  NA,  NA,  NA,

	   FA,  NA,  NA,  NA,  NA,  NA,  NA,
	   FA,  NA,  NA,  NA,  NA,  NA,  NA,
	   FA,  NA,  NA,  NA,  NA,  NA,  NA,
};

enum grammar_event{
	variable_use = 1,
	variable_assign = 3,
	variable_dereference = 4,
	type_use = 5,
	string_use = 6,
};

class parcer{
	std::unordered_map<std::string, part_of_speach*> variables_map;
public:
	parcer(std::unordered_map<std::string, part_of_speach*>& var_map){
		variables_map = var_map;
	}
	~parcer()=default;
	
	void parce(std::vector<syntax_node> nodes){
		int i = 0;
		int temp = nodes[i].numeric_id;
		int state = 0;
		int prev_state;
		while(i < nodes.size() && state != DA){
			//std::cout << "[" << state << "][" << temp << "] = ";
			temp = nodes[i].numeric_id;
			state = transition_table[state][temp];
			//std::cout << state << std::endl;

			if(state == FA){
				throw(std::string("Error: Parcer hit invalid state.\n"));
			}

			if(state == NA && prev_state > 0){
				switch(prev_state){
					case variable_use:
						if(variables_map.count(nodes[i - 1].code) == 1){
							part_of_speach* temp = variables_map[nodes[i - 1].code];
							auto& deref = *(temp);
							std::cout << deref() << " ";
						}else{
							std::cout << "Error: variable " << nodes[i - 1].code << " is undefined." << std::endl;
						}
						break;
					case variable_assign:
						if(type_map.count(nodes[i - 1].code) == 1){
							std::function<part_of_speach*()> type = type_map[nodes[i - 1].code];
							part_of_speach* TypeObject = type();

							if(variables_map.count(nodes[i - 3].code) == 1){ //delete old value
								part_of_speach* temp = variables_map[nodes[i - 3].code];
								delete temp;
								variables_map[nodes[i - 3].code] = TypeObject;
							}else{
								variables_map.emplace(nodes[i - 3].code, TypeObject);
							}
							std::cout << nodes[i - 3].code << " is a " << nodes[i - 1].code << std::endl;
						}else{
							std::cout << "Error: Unknown type " << nodes[i - 1].code << " in initialization of " << nodes[i - 3].code << std::endl;
						}
						break;
					case variable_dereference:
						//check object map
						//attempt to call code for object
						std::cout << nodes[i - 1].code << " is a dereference\n";
						break;
					case type_use:
						if(type_map.count(nodes[i - 1].code) == 1){
							std::function<part_of_speach*()> type = type_map[nodes[i - 1].code];
							part_of_speach* TypeObject = type();
							auto& deref = *(TypeObject);
							std::cout << deref() << " ";
							delete TypeObject;
						}else{
							std::cout << "Unknown type " << nodes[i - 1].code << std::endl;
						}
						break;
					case string_use:
						std::cout << nodes[i - 1].code;
						break;
					default:
						std::cout << "Unknown grammar_event\n";
				}
				state = prev_state = 0;
			}else if(state == NA){
				i++;
				state = 0;
			}else{
				i++;
			}

			prev_state = state;
		}
	}
};

#endif // PARCER_HPP