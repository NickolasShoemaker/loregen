#include <iostream>
#include <string>
#include <unordered_map>

#include "tokenizer.hpp"
#include "lexical.hpp"
#include "parcer.hpp"

//all lowercase is variable
//capital then all lowercase is type
//() to send arguments to type

/*
* say = {
*	name = Noun(male)
*	name " is a " name.gender ". yes, " name.pronoun " is."	
* }
*/

int main(int argc, char const *argv[]){
	std::unordered_map<std::string, part_of_speach> object_map;

	std::string command("");
	fdsm tokenizer;
	parcer parcer_engion;

	while(command != "exit"){
		std::cout << "\nLoreGen> ";
		getline(std::cin, command);
		parcer_engion.parce(tokenizer.lexical_analysis(command));
	}

	return 0;
}