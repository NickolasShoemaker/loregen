/**********************************************************************
* This is a definition of the Finite Deterministic State Machine used *
* by the LoreGen interpreter in the scanning phase.                   *
*                                                                     *
*                                                                     *
*                                                                     *
*                                                                     *
***********************************************************************/
#ifndef TOKENIZER_HPP
#define TOKENIZER_HPP

#include <iostream>
#include <vector>
#include <string>

#define NA -1 // This is the designated code to kick out of the state table, e.g. read a character not fitting with our state
#define FA -2 // The scanner has read a character that should not be in the file

/*
* This table lists what state to go to next from the current given an askii code
*
*/
static int state_transition_table[][128] = {
//				NU, SH, ST, ET, EO, EQ, AK, BL, BS, TB, NL, VT, FF, CR, SO, SI, DL, D1, D2, D3, D4, NK, SY, EB, CN, EM, SB, EC, FS, GS, RS, US, SP,  !,  ",  #,  $,  %,  &,  ',  (,  ),  *,  +,  ,,  -,  .,  /,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  :,  ;,  <,  =,  >,  ?,  //	 		 	 @,  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,  P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,  [,  \,  ],  ^,  _,  //  			 `,  a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o,  p,  q,  r,  s,  t,  u,  v,  w,  x,  y,  z,  {,  |,  },  ~, DL,
/*num_lit_s3*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA,  5, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  3, NA, NA, /*num_lit_s3*/  NA,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, NA, NA, NA, NA, FA,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
/*num_lit_s3*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  2, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, NA, NA, NA, NA, FA,
/*num_lit_s3*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  2, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, NA, NA, NA, NA, FA,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
/*startpar22*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*endcurly41*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*endcurly41*/  NA,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, NA, NA, NA, NA, NA, /*endcurly41*/  NA,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, NA, NA, NA, NA, FA,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
/*startpar22*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA,  5,  5,  6,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5, /*startpar22*/   5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5, /*startpar22*/   5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5, FA,
/*startpar22*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,	
};

enum token{
	variable = 1,
	dereference = 2,

	assign = 3,

	type = 4,
	//artifact, 	//a complex definition that can be invoked
	string = 6, 	//this is just a helper feature

	end_of_input = 5,
};

/*
* This holds information about our tokens as we abstract
*/
struct syntax_node{
	std::string code;
	int numeric_id;
	int line;

	syntax_node(std::string c, int ni, int l){
		code = c;
		numeric_id = ni;
		line = l;
	}
};

/*
* fdsm Finite Deterministic State Machine or automata
*
* This class handles a state machine for combinations of printable askii
* characters and the tokens that match those patterns
*/
class fdsm{
	int line; //the line the token ends on, we need to store this between calls
public:
	fdsm(){
		line = 1;
	}

	/*
	* Wrapper that doesn't need to handle blocks.
	* This is for the prompt when input is likely small
	*/
	std::vector<syntax_node> lexical_analysis(std::string code){
		int unimportant;
		return std::move(lexical_analysis(code, unimportant));
	}
	/*
	* This function analyses a codes progress though the state machine
	* keeping track of its current and previous state. Using a failure
	* state NA(-1) it determines when a certain token is done and saves
	* the previous state to be returned.
	*
	* TODO: line numbers drift when we change blocks, figure out why.
	*/
	std::vector<syntax_node> lexical_analysis(std::string code, int &last_good_token, bool add_invalid=true){
		std::vector<syntax_node> end_states;
		std::string token_syntax;

		if(add_invalid)
			code.append(" ");	//ensure we reach a final end state

		int prev_state = 0;
		int state = 0;	//start in root state

		int temp = code[0];

		int i = 0;
		last_good_token = i;
		while(i < code.size()){
			prev_state = state;
			state = state_transition_table[prev_state][temp];
			//std::cout << "temp is " << temp << " state is " << state << " prev " << prev_state << std::endl;
			
            if(state == FA){
            	std::string err = "line " + std::to_string(line) + ":nonprintalbe character read by lexer. Askii code " + std::to_string(temp);
                throw(err);
            }

			if(state < 0 && prev_state > 0){			//we hit an invalid character

				syntax_node node_temp(token_syntax, prev_state, line);

				end_states.push_back(node_temp);
				last_good_token = i; //we want char in buffer that was the end of last token
				state = 0;
				prev_state = 0;
				token_syntax.clear();
			}else if(state <= 0 && prev_state == 0){	//start invalid
                if(temp == '\n')
                    line++;
                temp = code[++i];
				state = 0;
				token_syntax.clear();
			}else{
				token_syntax.push_back(code[i]);
                if(temp == '\n')
                    line++;
				temp = code[++i];			            //eat current character
			}
		}

		syntax_node node_temp("token_syntax", end_of_input, line);
		end_states.push_back(node_temp);

		return std::move(end_states);
	}
};

#endif //TOKENIZER_HPP