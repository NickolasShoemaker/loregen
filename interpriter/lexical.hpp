#ifndef LEXICAL_HPP
#define LEXICAL_HPP

#include <string>

class part_of_speach{
public:
	part_of_speach()=default;
	virtual std::string operator()(){}
};

enum Gender{
	male,
	female,
	neuter
};

class Noun: public part_of_speach{
	//Proper: starts with capital
	//Countable
	Gender gender;
	std::string word;
public:
	Noun()=default;

	std::string operator()(){
		return std::string("tree");
	};
};
class Verb: public part_of_speach{
	std::string word;
public:
	Verb()=default;

	std::string operator()(){
		return std::string("runs");
	};
};
class Adverb: public part_of_speach{
	std::string word;
public:
	Adverb()=default;

	std::string operator()(){
		return std::string("carefully");
	};
};
class Adjective: public part_of_speach{
	std::string word;
public:
	Adjective()=default;

	std::string operator()(){
		return std::string("tall");
	};
};
class Preposition: public part_of_speach{
	std::string word;
public:
	Preposition()=default;

	std::string operator()(){
		return std::string("under");
	};
};
class Conjunction: public part_of_speach{
	std::string word;
public:
	Conjunction()=default;

	std::string operator()(){
		return std::string("and");
	};
};
class Article: public part_of_speach{
	std::string word;
public:
	Article()=default;

	std::string operator()(){
		return std::string("The");
	};
};

#endif //LEXICAL_HPP