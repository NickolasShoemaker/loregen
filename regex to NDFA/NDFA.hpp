/********************************************************************
* Nick Shoemaker                                                    *
*                                                                   *
* This file implements a non-deterministic finite automata.         *
* This can be used to generate random output following certain      *
* patterns.                                                         *
********************************************************************/
#include <unordered_map>
#include <array>
#include <vector>

template<typename Map_state_id, typename State_table_type, int State_size>
class NDFA{
	std::unordered_map<Map_state_id, std::array<State_table_type,State_size>> state_machine;
public:
	//------------------------------------------
	// Functions to create the NDFA
	//------------------------------------------

	/*
	* Add a rule based on a string of characters
	*/
	void add_rule(Map_state_id map, std::string code){
		std::unordered_map<State_table_type,int> temp;

		for(auto& character : code){
			if(temp.count(character) == 1){
				temp[character]++;
			}else{
				temp.emplace(character, 1);
			}
		}
		add_rule(map, temp);
	}

	/*
	* Calculate the tickets from a collection of chars
	* then add rule
	*/
	void add_rule(Map_state_id map, std::vector<State_table_type> charclass){
		std::unordered_map<State_table_type,int> temp;

		for(auto& character : charclass){
			if(temp.count(character) == 1){
				temp[character]++;
			}else{
				temp.emplace(character, 1);
			}
		}

		add_rule(map, temp);
	}

	/*
	* Add rule with a collection of char to ticket mappings
	*/
	void add_rule(Map_state_id map, std::unordered_map<State_table_type,int> rules){
		std::array<State_table_type,State_size> tickets;

		//Count the total number of characters in this rule
		int total = 0;
		for(auto& count : rules){
			total += count.second;
		}

		//Convert to percent chance of character showing up
		int percent = 0;
		int ticket_point = 0;
		for(auto i = rules.begin(); i != rules.end(); i++){
			percent = (i->second * State_size / total);
			for(int j = ticket_point; j < percent + ticket_point; j++){
				tickets[j] = i->first;
			}
			ticket_point += percent;
		}

		//fill out end of array with last char
		//this ensures we don't grab null chars
		for(int j = ticket_point; j < tickets.size(); j++){
			tickets[j] = tickets[j - 1];
		}

		//create a state from tickets and link to map
		state_machine.emplace(map, tickets);
	}

	//------------------------------------------
	// Functions to use the NDFA
	//------------------------------------------

	/*
	* Get the next state based on a current state and an index
	* This can be used to either get a specific value or a random
	* one in a particular state if index is random.
	*/
	Map_state_id get_state(Map_state_id state, int index){
		return state_machine[state].at(index % State_size);
	}

	void debug_print(){
		for(auto& state : state_machine){
			std::cout << state.first << ": [" << state.second.begin() << "]\n";
		}
	}
};