/**************************************************************
* Nick Shoemaker                                              *
*                                                             *
* This file uses a non-deterministic automata to generate     *
* names following a certain pattern.                          *
***************************************************************/
#include <random>
#include <unordered_map>
#include <string>
#include <iostream>

namespace name_generator{

	/*
	* Keeps chances to go to next states
	*/
	class non_deterministic_rule{
		char tickets[100];
	public:
		non_deterministic_rule(std::vector<std::pair<char,int>> rules){
			int total = 0;
			for(int i = 0; i < rules.size(); i++){
				total += rules[i].second;
			}

			int percent = 0;
			int ticket_point = 0;
			for(int i = 0; i < rules.size(); i++){
				percent = (rules[i].second * 100 / total);
				for(int j = ticket_point; j < percent + ticket_point; j++){
					tickets[j] = rules[i].first;
				}
				ticket_point += percent;
			}

			//fill out end of array with last char
			//this ensures we don't grab null chars
			for(int i = ticket_point; i < 100; i++){
				tickets[i] = rules[rules.size() - 1].first;
			}
		}
		//create null rule
		non_deterministic_rule(){
			for(int i = 0; i < 100; i++){
				tickets[i] = -1;
			}
		}

		int get_next_state(int key){
			return tickets[key % 100];
		}
	};

	non_deterministic_rule null_rule;  //posibly could just do 100 -1
	non_deterministic_rule start_rule({{'A',7},{'B',6},{'C',7},{'D',8},{'E',6},{'F',3},{'G',4},{'H',4},{'I',1},{'J',8},{'K',3},{'L',6},{'M',7},{'N',2},{'O',2},{'P',2},{'Q',1},{'R',8},{'S',5},{'T',4},{'U',0},{'V',1},{'W',4},{'X',0},{'Y',0},{'Z',1}}); //start rule
	//non_deterministic_rule start_rule({{'A',87},{'B',71},{'C',87},{'D',97},{'E',74},{'F',36},{'G',50},{'H',46},{'I',18},{'J',100},{'K',36},{'L',71},{'M',88},{'N',29},{'O',20},{'P',22},{'Q',5},{'R',94},{'S',64},{'T',53},{'U',1},{'V',17},{'W',44},{'X',1},{'Y',2},{'Z',6}});

	non_deterministic_rule A({{'a',1},{'b',5},{'d',6},{'g',1},{'h',2},{'l',26},{'m',3},{'n',19},{'r',15},{'s',2},{'u',6},{'v',1}});
	non_deterministic_rule B({{'a',6},{'e',15},{'i',3},{'l',3},{'o',8},{'r',27},{'u',8},{'y',1}});
	non_deterministic_rule C({{'a',18},{'e',4},{'h',20},{'l',19},{'o',18},{'r',4},{'u',2},{'y',2}});
	non_deterministic_rule D({{'a',35},{'e',26},{'i',6},{'o',20},{'r',1},{'u',5},{'w',3},{'y',1}});
	non_deterministic_rule E({{'a',3},{'d',13},{'f',2},{'l',21},{'m',10},{'n',2},{'r',12},{'s',1},{'t',1},{'u',3},{'v',3},{'z',3}});
	non_deterministic_rule F({{'a',3},{'e',7},{'i',2},{'l',4},{'o',3},{'r',17}});
	non_deterministic_rule G({{'a',15},{'e',10},{'i',6},{'l',2},{'o',2},{'r',10},{'u',5}});
	non_deterministic_rule H({{'a',15},{'e',9},{'i',4},{'o',10},{'u',7},{'y',1}});
	non_deterministic_rule I({{'a',1},{'g',1},{'k',1},{'r',4},{'s',9},{'v',2}});
	non_deterministic_rule J({{'a',30},{'c',1},{'e',27},{'i',3},{'o',30},{'u',9}});
	non_deterministic_rule K({{'a',3},{'e',18},{'i',6},{'o',2},{'r',4},{'u',2},{'y',1}});
	non_deterministic_rule L({{'a',15},{'e',22},{'i',6},{'l',1},{'o',12},{'u',10},{'y',5}});
	non_deterministic_rule M({{'a',43},{'c',1},{'e',6},{'i',21},{'o',14},{'u',1},{'y',2}});
	non_deterministic_rule N({{'a',5},{'e',7},{'i',6},{'o',10},{'u',1}});
	non_deterministic_rule O({{'c',1},{'d',2},{'l',4},{'m',2},{'r',4},{'s',3},{'t',3},{'w',1}});
	non_deterministic_rule P({{'a',9},{'e',5},{'h',3},{'i',1},{'o',2},{'r',2}});
	non_deterministic_rule Q({{'u',5}});
	non_deterministic_rule R({{'a',18},{'e',14},{'h',1},{'i',11},{'o',37},{'u',12},{'y',1}});
	non_deterministic_rule S({{'a',15},{'c',4},{'e',5},{'h',13},{'i',4},{'o',4},{'p',1},{'t',15},{'u',1},{'y',2}});
	non_deterministic_rule T({{'a',3},{'e',8},{'h',8},{'i',4},{'o',11},{'r',11},{'u',1},{'y',7}});
	non_deterministic_rule U({{'l',1}});
	non_deterministic_rule V({{'a',6},{'e',2},{'i',8},{'o',1}});
	non_deterministic_rule W({{'a',12},{'e',6},{'h',1},{'i',22},{'m',1},{'o',1},{'y',1}});
	non_deterministic_rule X({{'a',1}});
	non_deterministic_rule Y({{'o',2}});
	non_deterministic_rule Z({{'a',6}});

	non_deterministic_rule a({{'a',2},{'b',3},{'c',24},{'d',21},{'e',7},{'f',1},{'g',1},{'h',11},{'i',15},{'j',1},{'k',2},{'l',60},{'m',36},{'n',140},{'p',2},{'q',1},{'r',132},{'s',29},{'t',18},{'u',17},{'v',11},{'w',4},{'x',3},{'y',22},{'z',1},{-1,13}});
	non_deterministic_rule b({{'a',4},{'b',4},{'d',1},{'e',26},{'i',7},{'l',2},{'r',5},{'t',1},{'u',3},{'y',6},{-1,5}});
	non_deterministic_rule c({{'a',5},{'c',1},{'e',33},{'h',20},{'i',13},{'k',34},{'o',17},{'q',1},{'t',4},{'u',2},{'y',5},{-1,12}});
	non_deterministic_rule d({{'a',10},{'d',8},{'e',23},{'f',1},{'g',4},{'i',7},{'l',3},{'m',3},{'n',3},{'o',45},{'r',20},{'s',3},{'u',2},{'w',4},{'y',12},{-1,88}});
	non_deterministic_rule e({{'a',14},{'b',5},{'c',3},{'d',27},{'e',7},{'f',9},{'g',7},{'h',1},{'i',7},{'j',1},{'k',2},{'l',84},{'m',10},{'n',88},{'o',16},{'p',4},{'q',1},{'r',143},{'s',38},{'t',21},{'u',2},{'v',12},{'w',12},{'x',5},{'y',46},{'z',1},{-1,145}});
	non_deterministic_rule f({{'a',2},{'e',4},{'f',9},{'i',2},{'o',14},{'r',10},{'t',1},{'u',2},{-1,6}});
	non_deterministic_rule g({{'a',5},{'e',12},{'g',2},{'h',5},{'i',7},{'l',2},{'n',1},{'o',8},{'u',5},{-1,20}});
	non_deterministic_rule h({{'a',44},{'e',27},{'i',8},{'l',1},{'m',2},{'n',8},{'o',9},{'r',4},{'t',1},{'u',5},{'y',1},{-1,35}});
	non_deterministic_rule i({{'a',27},{'b',3},{'c',51},{'d',8},{'e',48},{'f',4},{'g',11},{'j',1},{'k',4},{'l',56},{'m',10},{'n',84},{'o',32},{'p',5},{'q',3},{'r',11},{'s',41},{'t',16},{'u',6},{'v',1},{'x',1},{-1,7}});
	non_deterministic_rule j({{'a',3},{'o',1}});
	non_deterministic_rule k({{'a',1},{'e',11},{'i',5},{'l',2},{'o',1},{'s',2},{'u',1},{'y',3},{-1,32}});
	non_deterministic_rule l({{'a',36},{'b',13},{'c',2},{'d',26},{'e',58},{'f',13},{'i',49},{'k',1},{'l',56},{'m',7},{'n',1},{'o',23},{'p',6},{'r',1},{'s',3},{'t',13},{'u',3},{'v',11},{'w',1},{'y',8},{-1,105}});
	non_deterministic_rule m({{'a',30},{'b',4},{'e',26},{'i',22},{'m',12},{'o',17},{'s',1},{'u',6},{'y',7},{-1,16}});
	non_deterministic_rule n({{'a',26},{'c',24},{'d',48},{'e',45},{'f',4},{'g',18},{'h',2},{'i',30},{'j',1},{'k',5},{'l',2},{'n',30},{'o',15},{'r',4},{'s',7},{'t',42},{'u',5},{'v',2},{'w',2},{'y',14},{'z',5},{-1,237}});
	non_deterministic_rule o({{'a',3},{'b',17},{'c',4},{'d',24},{'e',8},{'f',2},{'g',3},{'h',10},{'i',2},{'k',2},{'l',38},{'m',17},{'n',143},{'o',9},{'p',5},{'r',62},{'s',23},{'t',7},{'u',10},{'v',3},{'w',3},{'y',15},{-1,131}});
	non_deterministic_rule p({{'e',9},{'h',14},{'o',3},{'p',1},{-1,3}});
	non_deterministic_rule q({{'u',8},});
	non_deterministic_rule r({{'a',47},{'b',5},{'c',11},{'d',34},{'e',85},{'f',2},{'g',7},{'i',70},{'k',6},{'l',24},{'m',15},{'n',22},{'o',44},{'q',1},{'r',43},{'s',9},{'t',44},{'u',5},{'v',9},{'w',4},{'y',34},{-1,62}});
	non_deterministic_rule s({{'a',9},{'c',5},{'e',23},{'h',9},{'i',5},{'l',3},{'m',3},{'o',19},{'p',3},{'q',1},{'r',2},{'s',11},{'t',39},{'u',2},{'v',1},{'w',2},{-1,85}});
	non_deterministic_rule t({{'a',9},{'c',4},{'e',28},{'h',25},{'i',20},{'n',2},{'o',66},{'r',4},{'t',20},{'u',5},{'w',1},{'y',5},{'z',1},{-1,61}});
	non_deterministic_rule u({{'a',10},{'b',5},{'c',8},{'d',11},{'e',14},{'f',2},{'g',12},{'i',12},{'k',1},{'l',9},{'m',4},{'n',13},{'p',3},{'r',21},{'s',37},{'t',1},{'y',1},{'z',1},{-1,2}});
	non_deterministic_rule v({{'a',10},{'e',16},{'i',26},{'o',4},});
	non_deterministic_rule w({{'a',9},{'e',7},{'i',8},{'n',2},{'o',6},{'r',1},{'t',1},{-1,5}});
	non_deterministic_rule x({{'a',1},{'i',2},{'t',1},{'w',1},{-1,4}});
	non_deterministic_rule y({{'a',5},{'c',3},{'d',7},{'e',1},{'f',1},{'l',13},{'m',6},{'n',10},{'o',1},{'r',8},{'s',3},{'t',2},{'w',1},{-1,142}});
	non_deterministic_rule z({{'a',2},{'e',2},{'o',4},{'r',1},{-1,3}});

	std::unordered_map<char,non_deterministic_rule> NFSM({{'A',A}, {'B',B}, {'C',C}, {'D',D}, {'E',E}, {'F',F}, {'G',G}, {'H',H}, {'I',I}, {'J',J}, {'K',K}, {'L',L}, {'M',M}, {'N',N}, {'O',O}, {'P',P}, {'Q',Q}, {'R',R}, {'S',S}, {'T',T}, {'U',U}, {'V',V}, {'W',W}, {'X',X}, {'Y',Y}, {'Z',Z},
															{'a',a}, {'b',b}, {'c',c}, {'d',d}, {'e',e}, {'f',f}, {'g',g}, {'h',h}, {'i',i}, {'j',j}, {'k',k}, {'l',l}, {'m',m}, {'n',n}, {'o',o}, {'p',p}, {'q',q}, {'r',r}, {'s',s}, {'t',t}, {'u',u}, {'v',v}, {'w',w}, {'x',x}, {'y',y}, {'z',z}});

	enum gender{
		neuter = 0,
		male = 1,
		female = 2,
		it = 3,
	};

	class Name{
		int gender;
		std::string name;
		std::minstd_rand0 random_gen;

		std::string generate_name(int min_len){
			std::string name;
			char state = start_rule.get_next_state(random_gen());
			while(state != -1 || name.size() < min_len){
				if(state == -1){
					while(state == -1){
						state = NFSM[name.back()].get_next_state(random_gen());	
					}
					name.push_back(state);
				}else{
					name.push_back(state);
					state = NFSM[state].get_next_state(random_gen());
				}
			}
			return name;
		}
	public:
		Name(unsigned s1, int gen, int min_len=3){
			random_gen.seed(s1);
			name = generate_name(min_len);
			gender = gen;
		}
		Name(int min_len=3){
			random_gen.seed(s1);			//set seed by clock
			name = generate_name(min_len);
			gender = gen;					//set gender statistically
		}

		std::string get_pronoun(){
			if(gender == 0){
				return "Ze";
			}else if(gender == 1){
				return "He";
			}else if(gender == 2){
				return "She";
			}else{
				return "It";
			}
		}

		std::string get_name(){
			return name;
		}
	};

}

int main(){
	for(int i = 0; i < 20; i++){
		name_generator::Name n(i,i % 4);
		std::cout << n.get_name() << " identifies as " << n.get_pronoun()  << std::endl;
	}

	return 0;
}