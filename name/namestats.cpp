/**************************************************************
* Nick Shoemaker                                              *
*                                                             *
* This file is a helper that can create a program that creates*
* the data for the non-deterministic automata for the namegen *
***************************************************************/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

int array[26][26] = {0};
int next[26][26] = {0};
int first[26] = {0};
int last[26] = {0};

/*
* Get an integer value for an askii character
* This makes it easy to store in an array
*/
inline int convert(char key){
	if(key >= 'A' && key <= 'Z')
		return key - 'A';
	else if(key >= 'a' && key <= 'z')
		return key - 'a';
	else
		std::cout << "error got " << (int)key << std::endl;
}

/*
* Prints information as a table rather than a list
* Helpful for debugging
*/
void print_table(){
	std::cout << "First letters:\n";
	for(int i=65; i < 91; i++){
		std::cout << std::setw(4) << (char)i;
	}
	std::cout << "\n";
	for (int i = 0; i < 26; i++){
		std::cout << " " << std::setw(3) << first[i];
	}
	std::cout << std::endl << std::endl;

	std::cout << "\n\nTransition letters:\n   ";
	for(int i=97; i < 123; i++){
		std::cout << std::setw(4) << (char)i;
	}
	std::cout << "\n";
	for (int i = 0; i < 26; i++){
		std::cout << std::setw(3) << (char)(i+97);
		for (int j = 0; j < 26; j++){
			std::cout << " " << std::setw(3) << array[i][j];
		}
		std::cout << "\n";
	}

	std::cout << "\n\nNext\n   ";
	for(int i=97; i < 123; i++){
		std::cout << std::setw(4) << (char)i;
	}
	std::cout << "\n";
	for (int i = 0; i < 26; i++){
		std::cout << std::setw(3) << (char)(i+65);
		for (int j = 0; j < 26; j++){
			std::cout << " " << std::setw(3) << next[i][j];
		}
		std::cout << "\n";
	}
}

/*
* Output character combination patterns
*/
void print_list(){
	std::cout << "First letters:\n";
	for (int i = 0; i < 26; i++){
		std::cout << "{\'" << (char)(i + 65) << "\'," << first[i] << "},";
	}
	std::cout << "\n\n";

	std::cout << "Last letters:\n";
	for (int i = 0; i < 26; i++){
		std::cout << "{\'" << (char)(i + 97) << "\'," << last[i] << "},";
	}
	std::cout << "\n\n";

	std::cout << "Next\n";
	for (int i = 0; i < 26; i++){
		std::cout << (char)(i+65) << ": ";
		for (int j = 0; j < 26; j++){
			if(next[i][j] != 0)
				std::cout << "{\'" << (char)(j + 97) << "\'," << next[i][j] << "},";
		}
		std::cout << "\n";
	}

	std::cout << "\n\nTransition\n";
	for (int i = 0; i < 26; i++){
		std::cout << (char)(i+97) << ": ";
		for (int j = 0; j < 26; j++){
			if(array[i][j] != 0)
				std::cout << "{\'" << (char)(j + 97) << "\'," << array[i][j] << "},";
		}
		if(last[i] != 0)
			std::cout << "{-1," << last[i] << "}";	

		std::cout << "\n";
	}
}

/*
* Read in a text file of names and create a NDA of the character transitions
*/
int main(int argc, char const *argv[]){
	if (argc == 2){
		std::fstream file(argv[1]);
		std::string buffer;

		while(file.good()){
			getline(file, buffer);

			char current = buffer[0];
			first[convert(current)]++;

			next[convert(current)][convert(buffer[1])]++;
			current = buffer[1];

			for(int i = 2; i < buffer.size(); i++){
			//	std::cout << convert(current) << " " << convert(buffer[i]) << ", ";
				array[convert(current)][convert(buffer[i])]++;
				current = buffer[i];
			}

			//calculate chance of being last
			last[convert(current)]++;
		}
		file.close();

		print_list();		
	}else{
		std::cout << "Usage: ./a.out <file of names>" << std::endl;
	}

	return 0;
}
